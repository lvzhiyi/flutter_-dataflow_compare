# BLoC - ScopedModel - Redux - fishRedux - fishReduxAdaptor - provider - Comparison

> 深入对比 flutter 数据流解决方案：包括：

-   `BLoC` 流式响应式编程数据流
-   `ScopedModel` 典型的基于`InheritedWidget`将`model`扩展其子代
-   `Redux` 经典的`redux`解决方案
-   `fishRedux` 阿里咸鱼技术数据流框架
-   `fishReduxAdaptor` 引入`Adaptor` 的 `fish-redux` 解决方案
-   `provider` 谷歌推荐的数据流方案

<!-- Sample application to illustrate the article available on [didierboelens.com](https://www.didierboelens.com/2019/04/bloc---scopedmodel---redux---comparison/). -->
